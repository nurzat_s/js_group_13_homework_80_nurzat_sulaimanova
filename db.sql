create schema inventory collate utf8_general_ci;
use inventory;

create table categories
(
    id          int auto_increment
        primary key,
    name        varchar(255) not null,
    description varchar(255) null
);

create table locations
(
    id          int auto_increment
        primary key,
    name        varchar(255) not null,
    description varchar(255) null
);

create table items
(
    id          int auto_increment
        primary key,
    name        varchar(255) not null,
    description varchar(255) null,
    category_id int          not null,
    location_id int          not null,
    image       varchar(255) null,
    constraint items_categories_id_fk
        foreign key (category_id) references categories (id),
    constraint items_locations_id_fk
        foreign key (location_id) references locations (id)
);

insert into categories (id, name, description)
values  (1, 'Furniture', 'New ones'),
        (2, 'Appliances', 'For office'),
        (3, 'Computer equipment', 'Working');

insert into items (id, name, description, category_id, location_id, image)
values  (1, 'chair', 'leather chair', 1, 1, null),
        (2, 'laptop', 'apple macbook', 3, 2, null);

insert into locations (id, name, description)
values  (1, 'Director''s office', 'Take everything'),
        (2, 'Office 205', 'Only comps'),
        (3, 'Office 500', 'Only furniture');

