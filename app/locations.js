const express = require('express');
const db = require('../mySqlDb');

const router = express.Router();


router.get('/', async (req, res, next) => {
  try {
    let query = 'SELECT  * FROM locations';

    if (req.query.direction === 'desc') {
      query += ' ORDER BY id DESC'
    }


    let [locations] = await db.getConnection().execute(query);
    return res.send(locations);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const [locations] = await db.getConnection().execute('SELECT * FROM locations WHERE id = ?', [req.params.id]);

    const location = locations[0];

    if(!location) {
      return  res.status(404).send({message: 'Not found'});
    }

    return res.send(location);
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if(!req.body.name) {
      return res.status(400).send({message: 'Name required'});
    }

    const location = {
      name: req.body.name,
      description: req.body.description,
    };

    let query = 'INSERT INTO locations (name, description) VALUES (?, ?)';

    const [results] = await db.getConnection().execute(query, [
      location.name,
      location.description,
    ]);

    const id = results.insertId;

    console.log(results);

    return res.send({message: 'Created new location', id});
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    const [locations] = await db.getConnection().execute('DELETE  FROM locations WHERE id = ?', [req.params.id]);

    const location = locations[0];

    if(!location) {
      return  res.status(404).send({message: 'Not found'});
    }

    return res.send(location);
  } catch (e) {
    next(e);
  }
});

module.exports = router;