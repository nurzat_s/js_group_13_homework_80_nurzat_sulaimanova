const express = require('express');
const db = require('../mySqlDb');

const router = express.Router();


router.get('/', async (req, res, next) => {
  try {
    let query = 'SELECT  * FROM categories';


    if (req.query.direction === 'desc') {
      query += ' ORDER BY id DESC'
    }


    let [categories] = await db.getConnection().execute(query);
    console.log(categories)
    return res.send(categories);
  } catch (e) {
    next(e);
  }


});

router.get('/:id', async (req, res, next) => {
  try {
    const [categories] = await db.getConnection().execute('SELECT * FROM categories WHERE id = ?', [req.params.id]);

    const category = categories[0];

    if(!category) {
      return  res.status(404).send({message: 'Not found'});
    }

    return res.send(category);
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if(!req.body.name) {
      return res.status(400).send({message: 'Name is required'});
    }

    const category = {
      name: req.body.name,
      description: req.body.description
    };

    let query = 'INSERT INTO categories (name, description) VALUES (?, ?)';

    const [results] = await db.getConnection().execute(query, [
      category.name,
      category.description,
    ]);

    const id = results.insertId;

    return res.send({message: 'Created new category', id});
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    const [categories] = await db.getConnection().execute('DELETE  FROM categories WHERE id = ?', [req.params.id]);

    const category = categories[0];

    if(!category) {
      return  res.status(404).send({message: 'Not found'});
    }

    return res.send(category);
  } catch (e) {
    next(e);
  }
});




module.exports = router;