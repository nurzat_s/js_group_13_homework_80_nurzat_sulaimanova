const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    let query = 'SELECT  * FROM items';

    if (req.query.filter === 'image') {
      query += ' WHERE image IS NOT NULL'
    }

    if (req.query.orderBy === 'name' && req.query.direction === 'desc') {
      query += ' ORDER BY id DESC'
    }


    let [items] = await db.getConnection().execute(query);
    return res.send(items);
  } catch (e) {
    next(e);
  }


});

router.get('/:id', async (req, res, next) => {
  try {
    const [items] = await db.getConnection().execute('SELECT * FROM items WHERE id = ?', [req.params.id]);

    const item = items[0];

    if(!item) {
      return  res.status(404).send({message: 'Not found'});
    }

    return res.send(item);
  } catch (e) {
    next(e);
  }
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if(!req.body.name || !req.body.category || !req.body.location) {
      return res.status(400).send({message: 'Name, category and location are required'});
    }

    const item = {
      name: req.body.name,
      description: req.body.description,
      category: req.body.category,
      location: req.body.location,
      image: null,
    };

    if(req.file) {
      item.image = req.file.filename;
    }

    let query = 'INSERT INTO items (name, description, category, location, image) VALUES (?, ?, ?, ?, ?)';

    const [results] = await db.getConnection().execute(query, [
      item.name,
      item.description,
      item.category,
      item.location,
      item.image
    ]);

    const id = results.insertId;

    console.log(results);

    return res.send({message: 'Created new item', id});
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    const [items] = await db.getConnection().execute('DELETE  FROM items WHERE id = ?', [req.params.id]);

    const item = items[0];

    if(!item) {
      return  res.status(404).send({message: 'Not found'});
    }

    return res.send(item);
  } catch (e) {
    next(e);
  }
});

module.exports = router;